class AddVotesToGoals < ActiveRecord::Migration
  def change
    add_column :goals, :votes, :integer, :default => 0
  end
end
