class RemoveOrderFromGoals < ActiveRecord::Migration
  def change
    remove_column :goals, :order_num
  end
end
