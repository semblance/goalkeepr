class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.text :description
      t.datetime :due_date
      t.integer :order_num
      t.integer :user_id

      t.timestamps
    end
  end
end
