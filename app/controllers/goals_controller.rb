class GoalsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @goal = Goal.new
    @goals = current_user.goals.order("created_at ASC")
  end

  def create
    saved = false

    if params[:goal] && params[:goal][:due_date]
      params[:goal][:due_date] = params[:goal][:due_date].to_date
      goal = current_user.goals.new(params[:goal])

      if (saved = goal.save)
        redirect_to goals_path, :notice => "Goal created successfully!"
      end
    end
    redirect_to goals_path, :alert => "Unable to create goal." unless saved
  end

  def destroy
    goal = current_user.goals.find(params[:id])
    goal.destroy

    redirect_to goals_path, :notice => "Goal removed"
  end

  def vote
    goal = current_user.goals.find(params[:goal_id])
    goal.increment!(:votes)
    render :text => goal.votes
  end
end
