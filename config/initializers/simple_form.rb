SimpleForm.setup do |config|
  config.wrappers :tag => :p, :class => :base, :error_class => :error do |b|
      b.use :placeholder
      b.use :label_input
      b.use :hint,  :tag => :span, :class => "hint input"
      b.use :error, :tag => :span, :class => 'error input'
    end
end