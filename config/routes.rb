Goalkeepr::Application.routes.draw do
  get "pages/home"

  devise_for :users
  resources :goals, :only => [:index, :create, :destroy] do
    post 'vote'
  end

  root :to => 'pages#home'
end
